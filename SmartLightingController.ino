/*
 * Copyright (C) 2023
 *     Mark O'Donovan <shiftee@posteo.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Smart Lighting Controller
 *
 * Currently designed to run on Controllino Mega
 */

#include <Controllino.h>
#include <EEPROM.h>
#include <SPI.h>
#include <Ethernet.h>

#include <stdint.h>

const int32_t  SYS_NUM_INPUTS         = 21;
const int32_t  SYS_NUM_RELAYS         = 16;
const int32_t  SYS_NUM_OUTPUTS        = 24;
const uint16_t SYS_CONFIG_VERSION     = 1;        //config format version
const unsigned STR_BUF_SIZE           = 22;
const unsigned EEPROM_SIZE            = 4096;

struct random_buf_t                               //random byte buffer
{
#define RANDOM_BUF_SIZE 5
	uint8_t         buf[RANDOM_BUF_SIZE];             //byte buffer
	uint8_t         num_bytes;                        //number of bytes in buf
	uint8_t         max_bytes = 5;                    //max number of bytes in buf
};

struct string_buf_t                               //string buffer
{
	char            buf[STR_BUF_SIZE];                //string buffer
	uint8_t         size;                             //number of bytes in buf
};

enum modbus_exception_t {                         //modbus exception codes
	MODBUS_EXCEPTION_BAD_FUNC = 1,
	MODBUS_EXCEPTION_BAD_ADDR,
	MODBUS_EXCEPTION_BAD_VALUE,
	MODBUS_EXCEPTION_FAILURE,
	MODBUS_NUM_EXCEPTIONS
};

enum modbus_reg_misc_t {                          //misc modbus registers
	MODBUS_REG_DAYLIGHT,
	MODBUS_REG_TEMPERATURE,
	MODBUS_REG_ALL_OUTPUTS,
	MODBUS_REG_UPTIME_HI,
	MODBUS_REG_UPTIME_LO,
	MODBUS_REG_PROJECT_ID,
	MODBUS_NUM_MISC_REGS
};

enum input_type_t {                               //input types
	INPUT_SWITCH_PRESS,                               //retractive push light switch
	INPUT_SWITCH_TOGGLE,                              //toggle on/off light switch
	INPUT_SENSOR_MOTION,                              //motion sensor
	INPUT_SENSOR_TEMP,                                //temperature sensor
	INPUT_SENSOR_DAYLIGHT,                            //daylight sensor
	INPUT_NUM_TYPES
};

enum event_t {                                    //input event types
	EVENT_NONE,                                       //no event
	EVENT_PRESS,                                      //switch press
	EVENT_LONG_PRESS,                                 //long switch press
	EVENT_SENSOR_DETECT,                              //motion sensor detection
	EVENT_TEMP_HIGH,                                  //high temperature detected
	EVENT_DAYLIGHT_DAWN,                              //daylight detected
	EVENT_DAYLIGHT_DUSK,                              //daylight not detected
	EVENT_NUM_TYPES
};

enum screen_t {                                   //menu screen
	SCREEN_INPUT_SELECT,
	SCREEN_INPUT_EDIT,
	SCREEN_SET_TIMEOUT,
	SCREEN_OUTPUT_MAP,
	SCREEN_OUTPUT_UNMAP,
	SCREEN_MIRROR_INPUT,
	NUM_SCREENS
};

struct menu_t                                     //menu struct
{
	screen_t     screen;                              //menu screen
	int          input;                               //input index
	bool         refresh;                             //menu refresh requested
};

const int sys_inputs[SYS_NUM_INPUTS] = {
	CONTROLLINO_A0,
	CONTROLLINO_A1,
	CONTROLLINO_A2,
	CONTROLLINO_A3,
	CONTROLLINO_A4,
	CONTROLLINO_A5,
	CONTROLLINO_A6,
	CONTROLLINO_A7,
	CONTROLLINO_A8,
	CONTROLLINO_A9,
	CONTROLLINO_A10,
	CONTROLLINO_A11,
	CONTROLLINO_A12,
	CONTROLLINO_A13,
	CONTROLLINO_A14,
	CONTROLLINO_A15,
	CONTROLLINO_I16,
	CONTROLLINO_I17,
	CONTROLLINO_I18,
	CONTROLLINO_IN0,
	CONTROLLINO_IN1
};

const int sys_outputs[SYS_NUM_OUTPUTS] = {
	CONTROLLINO_D0,
	CONTROLLINO_D1,
	CONTROLLINO_D2,
	CONTROLLINO_D3,
	CONTROLLINO_D4,
	CONTROLLINO_D5,
	CONTROLLINO_D6,
	CONTROLLINO_D7,
	CONTROLLINO_D8,
	CONTROLLINO_D9,
	CONTROLLINO_D10,
	CONTROLLINO_D11,
	CONTROLLINO_D12,
	CONTROLLINO_D13,
	CONTROLLINO_D14,
	CONTROLLINO_D15,
	CONTROLLINO_D16,
	CONTROLLINO_D17,
	CONTROLLINO_D18,
	CONTROLLINO_D19,
	CONTROLLINO_D20,
	CONTROLLINO_D21,
	CONTROLLINO_D22,
	CONTROLLINO_D23
};

const int sys_relays[SYS_NUM_RELAYS] = {
	CONTROLLINO_R0,
	CONTROLLINO_R1,
	CONTROLLINO_R2,
	CONTROLLINO_R3,
	CONTROLLINO_R4,
	CONTROLLINO_R5,
	CONTROLLINO_R6,
	CONTROLLINO_R7,
	CONTROLLINO_R8,
	CONTROLLINO_R9,
	CONTROLLINO_R10,
	CONTROLLINO_R11,
	CONTROLLINO_R12,
	CONTROLLINO_R13,
	CONTROLLINO_R14,
	CONTROLLINO_R15
};

struct input_t                                    //input struct
{
	int          val;                                 //current input value
	int          val_prev;                            //previous input value
	uint32_t     event_tm;                            //used to track timing of input events
};

struct input_cnfg_t                               //input config struct
{
	input_type_t type;                                //input type
	uint32_t     timeout_ms;                          //relay switch-off timeout
	uint32_t     output_map;                          //bits 0-23 map to outputs (0-15 also to relays)
	uint8_t      mirrors_input;                       //input value is copied from this input
};

struct output_t                                   //output struct
{
	uint32_t     enable_tm;                           //enable time
	uint32_t     timeout_ms;                          //relay switch-off timeout
	int          val;                                 //output value
	event_t      enable_evt;                          //event which enabled the output
};

struct state_t                                    //system state struct
{
	input_t      input[SYS_NUM_INPUTS];               //system inputs
	output_t     output[SYS_NUM_OUTPUTS];             //system outputs/relays
	menu_t       menu;                                //serial menu
	int8_t       temp;                                //celcius temperature reading
	bool         daylight;                            //is it bright outside
	string_buf_t pkt_rx;                              //last modbus packet read
	string_buf_t pkt_tx;                              //last modbus packet sent
	byte         mac_addr[6];                         //ethernet mac address
	bool         network_configured;                  //is ethernet setup and has address
	random_buf_t random_bytes;                        //buffer for storing random bytes
};

struct config_t                                   //system config struct
{
	uint16_t        config_version;                   //configuration format version
	input_cnfg_t    input[SYS_NUM_INPUTS];            //system input configs
	uint16_t        modbus_tcp_port;                  //modbus tcp port
	uint16_t        dhcp_frequency;                   //how often we should try to get an IP (s)
};


const unsigned MODBUS_NUM_REGS = MODBUS_NUM_MISC_REGS + SYS_NUM_INPUTS + SYS_NUM_OUTPUTS;


//global variables
state_t        state;                             //system state
config_t       config;                            //system configuration
EthernetServer server(0);


/*
 * Formats the data in src buffer as a hex string
 *
 * Returns the number of chars
 */
int bufToHex(char* dest, uint8_t* src, unsigned num_bytes)
{
	//null terminate if num_bytes == 0
	dest[0] = 0;

	int chars = 0;

	for(unsigned i=0; i<num_bytes; i++)
		chars += sprintf(dest+i*2, "%02X", src[i]);

	return chars;
}

/*
 * Formats the data in src buffer as a hex string
 * and limits the size to the buffer size
 *
 * Returns the number of chars
 */
int packetToString(char* dest, uint8_t* src, unsigned num_bytes)
{
	unsigned max_bytes = STR_BUF_SIZE / 2;

	if( num_bytes >= max_bytes )
		num_bytes = max_bytes - 1;

	return bufToHex(dest, src, num_bytes);
}

/*
 * Print the system MAC address
 */
void printMAC()
{
	unsigned buf_size = 6*2;
	char buf[buf_size];
	bufToHex(buf, state.mac_addr, 6);

	for(int i=0; i<6; i++)
	{
		int index = i*2;
		Serial.write(buf+index, 2);
		if( i < 5 )
			Serial.print(":");
	}

	Serial.println();
}

/*
 * Load MAC address from EEPROM
 *
 * Returns 0 if a valid MAC is found and returned
 */
int loadMAC(uint8_t* mac, unsigned eeprom_size)
{
	uint8_t buf[10];

	int status = -1;

	for(unsigned i=0; i<10; i++)
	{
		unsigned idx = eeprom_size - 10 + i;

		buf[i] = eepromReadByte(idx);
	}

	uint32_t* magic = (uint32_t*) buf;
	bool valid_mac = *magic == 0x12341234;
	if( valid_mac )
	{
		for(unsigned i=0; i<6; i++)
		{
			mac[i] = buf[i+4];
		}

		status = 0;
	}

	return status;
}

/*
 * Save MAC address to EEPROM
 *
 * Returns 0 on success
 */
int saveMAC(uint8_t* mac, unsigned eeprom_size)
{
	int status = -1;

	for(unsigned i=0; i<6; i++)
	{
		unsigned idx = eeprom_size - 6 + i;

		status = eepromWriteByte(idx, mac[i]);

		if( status != 0 )
			break;
	}

	uint32_t magic = 0x12341234;

	for(unsigned i=0; i<4; i++)
	{
		unsigned idx = eeprom_size - 10 + i;

		uint8_t* ptr = (uint8_t*) &magic;

		status = eepromWriteByte(idx, ptr[i]);

		if( status != 0 )
			break;
	}

	return status;
}

/*
 * Returns true if it is bright outside
 */
bool isDaytime()
{
	return state.daylight;
}

/*
 * Read a byte from I2C EEPROM
 *     reg: register address
 */
uint8_t eepromReadByte(uint8_t reg)
{
	return EEPROM.read(reg);
}

/*
 * Write a byte to I2C EEPROM
 *     reg: register address
 *     val: value to write
 *
 * Returns 0 on success
 */
int eepromWriteByte(uint8_t reg, uint8_t val)
{
	int status = 1;

	if( reg < EEPROM.length() )
	{
		EEPROM.update(reg, val);
		status = 0;
	}

	return status;
}

/*
 * Calculates the CRC for num_bytes of buf
 */
uint16_t getCRC16(uint8_t* buf, unsigned num_bytes)
{
	uint16_t crc = 0xFFFF;

	for(unsigned i=0; i<num_bytes; i++)
	{
		crc ^= buf[i];

		for(int j=0; j<8; j++)
		{
			if( crc & 1 )
				crc = (crc >> 1) ^ 0xA001;
			else
				crc = (crc >> 1);
		}
	}

	return crc;
}

/*
 * Sets the default configuration
 */
void setDefaultConfig()
{
	config.config_version = SYS_CONFIG_VERSION;

	//configure inputs
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		config.input[i].type          = INPUT_SWITCH_PRESS;
		config.input[i].timeout_ms    = 8 * 60 * 60 * 1000L; //8 hours
		config.input[i].output_map    = 1L << i;
		config.input[i].mirrors_input = 0xFF;
	}

	config.modbus_tcp_port = 502;
	config.dhcp_frequency  = 1200; //20 mins
}

/*
 * Save the system configuration to EEPROM
 *
 * Returns 0 on success
 */
int saveConfig()
{
	unsigned num_bytes = sizeof(config_t);

	uint8_t* p = (uint8_t*) &config;

	for(unsigned i=0; i<num_bytes; i++)
	{
		uint8_t val = *(p+i);

		eepromWriteByte(i, val);
	}

	uint16_t crc = getCRC16(p, num_bytes);

	int status = 0;

	status += eepromWriteByte(num_bytes++, lowByte(crc));
	status += eepromWriteByte(num_bytes++, highByte(crc));

	return status;
}

/*
 * Loads the system configuration from EEPROM
 *
 * Returns 0 on success
 */
int loadConfig()
{
	unsigned num_bytes = sizeof(config_t);

	uint8_t* p = (uint8_t*) &config;

	for(unsigned i=0; i<num_bytes; i++)
	{
		p[i] = eepromReadByte(i);
	}

	uint16_t real_crc = getCRC16(p, num_bytes);

	uint8_t buf[2];
	buf[0] = eepromReadByte(num_bytes++);
	buf[1] = eepromReadByte(num_bytes++);

	uint16_t crc = buf[1] << 8 | buf[0];

	int status = 0;

	if( crc != real_crc )
	{
		setDefaultConfig();

		saveConfig();

		status = -1;
	}

	return status;
}

/*
 * Setup the digital inputs
 */
void setupInputs()
{
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		pinMode(sys_inputs[i], INPUT);
	}

	state.temp = -1;
}

/*
 * Setup the digital outputs
 */
void setupOutputs()
{
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
	{
		pinMode(sys_outputs[i], OUTPUT);
	}
}

/*
 * Read the digital/analog input at the supplied index
 *
 * Returns 0 on failure
 */
int readInput(unsigned index)
{
	int val = 0;

	if( index < SYS_NUM_INPUTS )
	{
		switch( config.input[index].type )
		{
		case INPUT_SENSOR_TEMP:
			val = analogRead( sys_inputs[index] );
			break;
		default:
			val = digitalRead( sys_inputs[index] );
		}
	}

	return val;
}

/*
 * Read a retractive light-switch input
 */
event_t readRetractiveSwitch(unsigned i)
{
	event_t evt = EVENT_NONE;

	bool input_gone_low  = state.input[i].val_prev != 0 && state.input[i].val == 0;
	bool input_gone_high = state.input[i].val_prev == 0 && state.input[i].val != 0;
	bool input_is_high   = state.input[i].val != 0;

	if( input_gone_high )
	{
		state.input[i].event_tm = millis();
	}

	if( input_gone_low )
	{
		if( state.input[i].event_tm )
		{
			evt = EVENT_PRESS;
		}
	}

	if( input_is_high )
	{
		if( state.input[i].event_tm )
		{
			uint32_t press_ms = millis() - state.input[i].event_tm;

			if( press_ms > 1250 )
			{
				evt = EVENT_LONG_PRESS;

				state.input[i].event_tm = 0;
			}
		}
	}

	return evt;
}

/*
 * Read a toggle on/off light-switch input
 *
 * On powerup if the input is high we return an event
 */
event_t readToggleSwitch(unsigned i)
{
	event_t evt = EVENT_NONE;

	bool input_gone_low  = state.input[i].val_prev != 0 && state.input[i].val == 0;
	bool input_gone_high = state.input[i].val_prev == 0 && state.input[i].val != 0;

	if( input_gone_high )
	{
		evt = EVENT_PRESS;
	}

	if( input_gone_low )
	{
		evt = EVENT_PRESS;
	}

	return evt;
}

/*
 * Read a motion sensor input
 */
event_t readMotionSensor(unsigned i)
{
	event_t evt = EVENT_NONE;

	bool input_is_high = state.input[i].val != 0;

	if( input_is_high )
	{
		evt = EVENT_SENSOR_DETECT;
	}

	return evt;
}

/*
 * Read a daylight sensor input
 */
event_t readDaylightSensor(unsigned i)
{
	event_t evt = EVENT_NONE;

	bool input_gone_low  = state.input[i].val_prev != 0 && state.input[i].val == 0;
	bool input_gone_high = state.input[i].val_prev == 0 && state.input[i].val != 0;

	if( input_gone_high )
	{
		evt = EVENT_DAYLIGHT_DAWN;
	}

	if( input_gone_low )
	{
		evt = EVENT_DAYLIGHT_DUSK;
	}

	return evt;
}


/*
 * Read the temperature sensor
 */
event_t readTempSensor(unsigned i)
{
	event_t evt = EVENT_NONE;

	int adc_val = state.input[i].val;

	//microvolts per adc step, calculated via multimeter,
	//use for up to 1V, use 14580 instead for 5V/12V signal levels
	uint32_t factor = 17500;

	uint32_t millivolts = (adc_val * factor) / 1000;

	int new_temp = millivolts / 10;
	int old_temp = state.temp;

	new_temp = (new_temp + old_temp) / 2; //average with previous value

	state.temp = new_temp;

	int threshold = 45;
	if( new_temp >= threshold && old_temp < threshold )
	{
		evt = EVENT_TEMP_HIGH;
	}

	return evt;
}

/*
 * Read all the digital inputs
 */
void readAllInputs()
{
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		state.input[i].val_prev = state.input[i].val;

		state.input[i].val      = readInput(i);

		//if current input mirrors another input then copy its value
		uint8_t master_input = config.input[i].mirrors_input;
		if( master_input < SYS_NUM_INPUTS )
			state.input[i].val = state.input[master_input].val;
	}
}

/*
 * Setup the relay outputs
 */
void setupRelays()
{
	for(int i=0; i<SYS_NUM_RELAYS; i++)
		pinMode(sys_relays[i], OUTPUT);
}

/*
 * Set the state of a relay
 */
void setRelay(unsigned index, bool val)
{
	if( index < SYS_NUM_RELAYS )
		digitalWrite( sys_relays[index], val );
}

/*
 * Write output values to relays
 */
void writeRelayOutputs()
{
	for(int i=0; i<SYS_NUM_RELAYS; i++)
		setRelay(i, state.output[i].val);
}

/*
 * Write output values to digital outputs
 */
void writeDigitalOutputs()
{
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
		digitalWrite( sys_outputs[i], state.output[i].val );
}

/*
 * Return an event for the passed input index
 */
event_t getInputEvent(unsigned i)
{
	event_t evt = EVENT_NONE;

	switch( config.input[i].type )
	{
	case INPUT_SWITCH_PRESS:
		evt = readRetractiveSwitch(i);
		break;
	case INPUT_SWITCH_TOGGLE:
		evt = readToggleSwitch(i);
		break;
	case INPUT_SENSOR_MOTION:
		evt = readMotionSensor(i);
		break;
	case INPUT_SENSOR_TEMP:
		evt = readTempSensor(i);
		break;
	case INPUT_SENSOR_DAYLIGHT:
		evt = readDaylightSensor(i);
		break;
	case INPUT_NUM_TYPES:
		break;
	}

	return evt;
}

/*
 * Handle the passed input event
 */
void handleInputEvent(input_cnfg_t& input, event_t evt)
{
	//do the same thing for each mapped output
	int common_val = -1;

	//update the values for each mapped output
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
	{
		uint32_t output_bit = 1L << i;

		bool output_is_mapped = input.output_map & output_bit;

		if( evt == EVENT_TEMP_HIGH )
			output_is_mapped = true;

		if( evt == EVENT_DAYLIGHT_DAWN || evt == EVENT_DAYLIGHT_DUSK )
			output_is_mapped = true;

		if( output_is_mapped )
		{
			auto& output = state.output[i];

			uint32_t timeout_ms = input.timeout_ms;

			bool output_was_on = output.val;

			switch( evt )
			{
			case EVENT_PRESS:
				if( common_val == -1 )
					common_val = ! output.val;
				output.val = common_val;
				break;
			case EVENT_LONG_PRESS:
				if( common_val == -1 )
					common_val = ! output.val;
				output.val  = common_val;
				timeout_ms = 0;  //long press disables timeout
				break;
			case EVENT_SENSOR_DETECT:
				if( ! isDaytime() )
					output.val = 1;
				break;
			case EVENT_TEMP_HIGH:
				output.val = 1;
				timeout_ms = 1000;
				break;
			case EVENT_DAYLIGHT_DAWN:
				state.daylight = true;
				break;
			case EVENT_DAYLIGHT_DUSK:
				state.daylight = false;
				break;
			case EVENT_NONE:
			case EVENT_NUM_TYPES:
				break;
			}

			bool output_event =
						evt == EVENT_PRESS ||
						evt == EVENT_TEMP_HIGH ||
						evt == EVENT_LONG_PRESS ||
						evt == EVENT_SENSOR_DETECT;

			//motion sensors should not activate lights or extend timeouts during the day
			if( evt == EVENT_SENSOR_DETECT && isDaytime() )
				output_event = false;

			if( output.val && output_event )
			{
				//motion sensor should not override timeout if light was already on
				bool preserve_timeout = output_was_on && evt == EVENT_SENSOR_DETECT;

				if( ! preserve_timeout )
				{
					output.timeout_ms = timeout_ms;
				}

				output.enable_tm  = millis();
				output.enable_evt = evt;
			}

			//at dawn we turn off any lights which were activated by motion sensor
			if( evt == EVENT_DAYLIGHT_DAWN )
				if( output.enable_evt == EVENT_SENSOR_DETECT )
					output.val = 0;
		}
	}
}

/*
 * Manage the outputs and relays for the lighting circuits
 */
void manageLightingSystem()
{
	//calculate output values
	for(int i=0; i<SYS_NUM_INPUTS; i++)
	{
		event_t evt = getInputEvent(i);

		handleInputEvent(config.input[i], evt);
	}

	//switch off outputs if timeouts have expired
	for(int i=0; i<SYS_NUM_OUTPUTS; i++)
	{
		auto& output = state.output[i];

		bool timeout_enabled = output.timeout_ms > 0;

		if( timeout_enabled )
		{
			if( millis() - output.enable_tm > output.timeout_ms )
			{
				output.val = 0;
			}
		}
	}
}

/*
 * Clear the serial screen buffer
 *
 * The Arduino Serial Monitor does not support proper screen clearing
 * commands so we simply print newlines to clear the screen.
 */
void clearScreen()
{
	for(int i=0; i<60; i++)
		Serial.println();

	state.menu.refresh = false;
}

/*
 * Tries to read an integer in range 0 - 999,999,999
 *
 * Returns -1 on failure
 */
int32_t tryReadInt32()
{
	if( ! Serial.available() )
		return -1;

	int32_t val = 0;

	for(int i=0; i<9; i++)
	{
		uint8_t b = Serial.read();

		if( b == 10 ) //enter
		{
			if( i == 0 )
				val = -1;
			break;
		}

		if( b < 48 || b > 57 )
			return -1;

		b -= 48;

		val *= 10;
		val += b;

		if( ! Serial.available() )
			break;
	}

	return val;
}

/*
 * Returns a string describing the passed input type
 */
const char* getInputTypeName(input_cnfg_t &input)
{
	switch( input.type )
	{
	case INPUT_SWITCH_PRESS:
		return "PRESS SWITCH";
	case INPUT_SWITCH_TOGGLE:
		return "TOGGLE SWITCH";
	case INPUT_SENSOR_MOTION:
		return "MOTION SENSOR";
	case INPUT_SENSOR_TEMP:
		return "TEMPERATURE SENSOR";
	case INPUT_SENSOR_DAYLIGHT:
		return "DAYLIGHT SENSOR";
	case INPUT_NUM_TYPES:
		break;
	}

	return "UNKNOWN";
}

/*
 * Switch to the passed screen in the serial UI
 */
void setScreen(screen_t screen)
{
	state.menu.screen  = screen;
	state.menu.refresh = true;
}

/*
 * Return the number of random bytes we have ready
 */
unsigned numRandomBytes()
{
	return state.random_bytes.num_bytes;
}

/*
 * Return the size of the random byte buffer
 */
unsigned maxRandomBytes()
{
	return state.random_bytes.max_bytes;
}

/*
 * Store a random byte for future use
 * This function should be called at unpredictable times
 * e.g on receiving serial data
 */
void gatherEntropy()
{
	auto num_bytes = numRandomBytes();

	if( num_bytes < maxRandomBytes() )
	{
		uint32_t tm = micros();

		uint8_t random_byte = tm & 0xFF;

		state.random_bytes.buf[num_bytes] = random_byte;
		state.random_bytes.num_bytes++;
	}
}

/*
 * Return a random byte from the byte buffer
 * If the buffer is empty then the value returned may be predictable
 */
uint8_t getRandomByte()
{
	uint8_t random_byte = micros() & 0xFF;

	auto num_bytes = numRandomBytes();

	if( num_bytes > 0 )
	{
		random_byte = state.random_bytes.buf[num_bytes];

		state.random_bytes.num_bytes--;
	}

	return random_byte;
}

/*
 * Allow user to select an input to edit
 */
void doMenuInputSelect()
{
	if( Serial.available() || state.menu.refresh )
	{
		int32_t val = tryReadInt32();

		if( val == -1 )
		{
			clearScreen();

			Serial.print("MAC: ");
			printMAC();

			if( isEthernetSetup() )
			{
				Serial.print("IP: ");
				Serial.println(Ethernet.localIP());
			}

			if( state.pkt_rx.size )
			{
				Serial.print("Rx: 0x");
				Serial.println(state.pkt_rx.buf);
			}

			if( state.pkt_tx.size )
			{
				Serial.print("Tx: 0x");
				Serial.println(state.pkt_tx.buf);
			}

			Serial.print("Daylight: ");
			if( isDaytime() )
				Serial.println("Yes");
			else
				Serial.println("No");

			if( state.temp >= 0 )
			{
				Serial.print("Temperature: ");
				Serial.print(state.temp);
				Serial.println(" C");
			}

			Serial.println("");
			Serial.println("#############################");
			Serial.println("# Configure Input    (0-20) #");
			Serial.println("# Show Configuration   (21) #");
			Serial.println("#############################");
			Serial.print("Enter a number: ");
		}

		if( val == 21 )
		{
			Serial.println("");
			Serial.print("                   ");
			Serial.println("TYPE      TIMEOUT   MAP    MIRRORS");

			for(int i=0; i<SYS_NUM_INPUTS; i++)
			{
				auto& input = config.input[i];

				char str[50];

				int chars = sprintf(str, "Input-%-2d %18s %8lu 0x%06lX   ",
											i,
											getInputTypeName(input),
											input.timeout_ms / 1000,
											input.output_map
											);
				Serial.write(str, chars);

				if( input.mirrors_input < SYS_NUM_INPUTS )
					Serial.println(input.mirrors_input);
				else
					Serial.println("-");
			}

			Serial.println(" ");
			Serial.print("DHCP frequency: ");
			Serial.print(config.dhcp_frequency);
			Serial.println("s");
		}

		if( val >= 0 && val < SYS_NUM_INPUTS )
		{
			setScreen(SCREEN_INPUT_EDIT);
			state.menu.input  = val;
		}
	}
}

/*
 * Allow user to edit an input
 */
void doMenuInputEdit()
{
	int idx = state.menu.input;

	auto& input = config.input[idx];

	if( Serial.available() || state.menu.refresh )
	{
		int32_t val = tryReadInt32();

		//handle commands
		if( val == 0 )
		{
			setScreen(SCREEN_INPUT_SELECT);
			return;
		}

		//change input type
		if( val == 1 )
		{
			if( input.mirrors_input >= SYS_NUM_INPUTS )
				input.type = input_type_t(input.type + 1);

			//only inputs A0 - A15 can read analog values
			if( input.type == INPUT_SENSOR_TEMP && idx > 15 )
				input.type = input_type_t(input.type + 1);

			if( input.type >= INPUT_NUM_TYPES )
				input.type = input_type_t(0);

			saveConfig();
		}

		if( val == 2 )
		{
			setScreen(SCREEN_SET_TIMEOUT);
			return;
		}

		if( val == 3 )
		{
			setScreen(SCREEN_OUTPUT_MAP);
			return;
		}

		if( val == 4 )
		{
			setScreen(SCREEN_OUTPUT_UNMAP);
			return;
		}

		if( val == 5 )
		{
			setScreen(SCREEN_MIRROR_INPUT);
			return;
		}


		//print screen data
		clearScreen();

		Serial.print("Input-");
		Serial.println(idx);
		Serial.print("       Type: ");
		Serial.println( getInputTypeName(input) );

		Serial.print("    Timeout: ");
		if( input.timeout_ms )
		{
			char unit = 's';

			uint32_t secs = input.timeout_ms / 1000;
			if( secs % 3600 == 0 )
			{
				Serial.print(secs / 3600 );
				unit = 'h';
			}
			else if( secs % 60 == 0 )
			{
				Serial.print(secs / 60 );
				unit = 'm';
			}
			else
			{
				Serial.print(secs);
			}

			Serial.println(unit);
		}
		else
		{
			Serial.println("Disabled");
		}

		Serial.print("     Outputs:");
		for(int i=0; i<SYS_NUM_OUTPUTS; i++)
		{
			if( input.output_map & (1L << i) )
			{
				Serial.print(" ");
				Serial.print(i);
			}
		}
		Serial.println();

		Serial.print("    Mirrors: ");
		if( input.mirrors_input < SYS_NUM_INPUTS )
			Serial.print(input.mirrors_input);

		Serial.println("\n\nPlease choose from the following options:");
		Serial.println("\t 0 - Return to main menu");
		Serial.println("\t 1 - Change input type");
		Serial.println("\t 2 - Change timeout");
		Serial.println("\t 3 - Add output to map");
		Serial.println("\t 4 - Remove output from map");
		if( input.mirrors_input < SYS_NUM_INPUTS )
			Serial.println("\t 5 - Disable input mirroring");
		else
			Serial.println("\t 5 - Mirror another input");
	}
}

/*
 * Allow user to set a timeout for outputs mapped to this input
 */
void doMenuSetTimeout()
{
	int idx = state.menu.input;

	auto& input = config.input[idx];

	if( Serial.available() || state.menu.refresh )
	{
		int32_t val = tryReadInt32();

		if( val == -1 )
		{
			clearScreen();
			Serial.println("24h : 86400s");
			Serial.println(" 8h : 28800s");
			Serial.println(" 2h :  7200s");
			Serial.println(" 1h :  3600s");
			Serial.println("30m :  1800s");
			Serial.println("15m :   900s");
			Serial.println();
			Serial.print("Enter timeout in seconds (0 = Disable): ");
		}
		else
		{
			input.timeout_ms = val * 1000;

			saveConfig();

			setScreen(SCREEN_INPUT_EDIT);
		}
	}
}

/*
 * Allow user to map an output to an input
 */
void doMenuOutputMap()
{
	int idx = state.menu.input;

	auto& input = config.input[idx];

	if( Serial.available() || state.menu.refresh )
	{
		int32_t val = tryReadInt32();

		if( val == -1 )
		{
			clearScreen();

			Serial.print("Enter Output/Relay Number (0-23): ");
		}

		if( val >= 0 && val < SYS_NUM_OUTPUTS )
		{
			uint32_t output_bit = 1L << val;

			input.output_map |= output_bit;

			saveConfig();

			setScreen(SCREEN_INPUT_EDIT);
		}
	}
}

/*
 * Allow user to unmap a output from an input
 */
void doMenuOutputUnmap()
{
	int idx = state.menu.input;

	auto& input = config.input[idx];

	if( Serial.available() || state.menu.refresh )
	{
		int32_t val = tryReadInt32();

		if( val == -1 )
		{
			clearScreen();

			Serial.print("Enter Output Number (0-23): ");
		}

		if( val >= 0 && val < SYS_NUM_OUTPUTS )
		{
			uint32_t output_bit = 1L << val;

			input.output_map &= ~output_bit;

			saveConfig();

			setScreen(SCREEN_INPUT_EDIT);
		}
	}
}

/*
 * Allow user to choose an input to mirror
 */
void doMenuMirrorInput()
{
	int idx = state.menu.input;

	auto& input = config.input[idx];

	if( input.mirrors_input < SYS_NUM_INPUTS )
	{
		input.mirrors_input = 0xFF;
		saveConfig();
		setScreen(SCREEN_INPUT_EDIT);
		return;
	}

	if( Serial.available() || state.menu.refresh )
	{
		int32_t val = tryReadInt32();

		if( val == -1 )
		{
			clearScreen();

			Serial.print("Enter input to mirror (0-20): ");
		}
		else
		{
			if( val >= 0 && val < SYS_NUM_INPUTS )
			{
				input.mirrors_input = val;
				input.type = config.input[val].type; //copy input type also
			}
			else
			{
				input.mirrors_input = 0xFF;
			}

			saveConfig();
			setScreen(SCREEN_INPUT_EDIT);
		}
	}
}

/*
 * Setup the serial user interface
 */
void setupSerialUI()
{
	Serial.begin(9600);

	state.menu.refresh = true;
}

/*
 * Manage the serial user interface
 */
void manageSerialUI()
{
	switch( state.menu.screen )
	{
	case SCREEN_INPUT_SELECT:
		doMenuInputSelect();
		break;
	case SCREEN_INPUT_EDIT:
		doMenuInputEdit();
		break;
	case SCREEN_SET_TIMEOUT:
		doMenuSetTimeout();
		break;
	case SCREEN_OUTPUT_MAP:
		doMenuOutputMap();
		break;
	case SCREEN_OUTPUT_UNMAP:
		doMenuOutputUnmap();
		break;
	case SCREEN_MIRROR_INPUT:
		doMenuMirrorInput();
		break;
	case NUM_SCREENS:
		break;
	}
}

/*
 * Prepare a modbus exception response
 *
 * Returns the response packet size in bytes
 */
int handleModbusException(uint8_t* buf, modbus_exception_t exception)
{
		buf[1] |= 0x80;
		buf[2] = exception;

		return 3;
}

/*
 * Prepare a modbus response packet in buf for modbus function 3
 * Ignores the first two bytes in the buffer and doesn't handle the CRC
 *
 * Returns the response packet size in bytes
 */
unsigned handleModbusFunc3(uint8_t* buf, uint16_t offset)
{
	unsigned count = buf[4] << 8 | buf[5];

	//check parameters
	bool bad_addr  = offset >= MODBUS_NUM_REGS;

	bool bad_count = count < 1 || count > MODBUS_NUM_REGS;

	unsigned last_reg = offset + count - 1;

	if( bad_addr || bad_count || last_reg >= MODBUS_NUM_REGS )
		return handleModbusException(buf, MODBUS_EXCEPTION_BAD_ADDR);

	//build response
	buf[2] = count * 2;

	unsigned num_bytes = 3;

	for(unsigned i=0; i<count; i++)
	{
		uint16_t val = 0;

		switch( static_cast<modbus_reg_misc_t>(offset) )
		{
		case MODBUS_REG_DAYLIGHT:
			val = state.daylight;
			break;
		case MODBUS_REG_TEMPERATURE:
			val = state.temp;
			break;
		case MODBUS_REG_ALL_OUTPUTS:
			for(int j=0; j<SYS_NUM_OUTPUTS; j++)
				if( state.output[j].val )
					val = 1;
			break;
		case MODBUS_REG_UPTIME_HI:
			val = millis() >> 16;
			break;
		case MODBUS_REG_UPTIME_LO:
			val = millis() & 0xFFFF;
			break;
		case MODBUS_REG_PROJECT_ID:
			val = 0x4433;
			break;
		case MODBUS_NUM_MISC_REGS:
			break;
		}

		unsigned inputs_idx  = MODBUS_NUM_MISC_REGS;
		unsigned outputs_idx = inputs_idx  + SYS_NUM_INPUTS;
		unsigned num_regs    = outputs_idx + SYS_NUM_OUTPUTS;

		if( offset >= inputs_idx && offset < outputs_idx )
			val = state.input[offset-inputs_idx].val;

		if( offset >= outputs_idx && offset < num_regs )
			val = state.output[offset-outputs_idx].val;

		buf[num_bytes++] = highByte(val);
		buf[num_bytes++] = lowByte(val);

		offset++;
	}

	return num_bytes;
}

/*
 * Prepare a modbus response packet in buf for modbus function 6
 * Ignores the first two bytes in the buffer and doesn't handle the CRC
 *
 * Returns the response packet size in bytes
 */
unsigned handleModbusFunc6(uint8_t* buf, uint16_t offset)
{
	uint16_t val = buf[4] << 8 | buf[5];

	int status = MODBUS_EXCEPTION_BAD_FUNC;

	if( offset >= MODBUS_NUM_REGS )
		status = MODBUS_EXCEPTION_BAD_ADDR;

	switch( static_cast<modbus_reg_misc_t>(offset) )
	{
	case MODBUS_REG_DAYLIGHT:
		break;
	case MODBUS_REG_TEMPERATURE:
		break;
	case MODBUS_REG_ALL_OUTPUTS:
		for(int i=0; i<SYS_NUM_OUTPUTS; i++)
			state.output[i].val = val;
		status = 0;
		break;

	case MODBUS_REG_UPTIME_HI:
	case MODBUS_REG_UPTIME_LO:
	case MODBUS_REG_PROJECT_ID:
	case MODBUS_NUM_MISC_REGS:
		break;
	}

	unsigned inputs_idx  = MODBUS_NUM_MISC_REGS;
	unsigned outputs_idx = inputs_idx + SYS_NUM_INPUTS;
	unsigned num_regs    = outputs_idx + SYS_NUM_OUTPUTS;

	if( offset >= inputs_idx && offset < outputs_idx )
	{
		state.input[offset-inputs_idx].val = val;
		status = 0;
	}

	if( offset >= outputs_idx && offset < num_regs )
	{
		state.output[offset-outputs_idx].val = val;
		status = 0;
	}

	unsigned num_bytes = 6;
	if( status )
		num_bytes = handleModbusException(buf, static_cast<modbus_exception_t>(status));

	return num_bytes;
}

/*
 * Handle any incoming modbus requests
 */
void manageModbusTCP()
{
	EthernetClient client = server.available();

	if( client )
	{
		const unsigned PKT_BUF_SIZE = 6 + 1 + 253;  //header + addr + modbus-pkt

		uint8_t buf[PKT_BUF_SIZE];

		unsigned num_bytes = 0;

		//wait for data
		uint32_t tm = millis();
		while( millis() - tm < 10 )
		{
			if( client.available() )
				break;
			delay(1);
		}

		//read data
		while( client.available() )
		{
			buf[num_bytes++] = client.read();

			if( num_bytes >= PKT_BUF_SIZE - 2 )
				break;

			if( ! client.available() )
				delay(1);
		}

		if( num_bytes > 6 )
		{
			//skip MBAP header
			num_bytes -= 6;

			uint8_t* pkt = buf+6;

			packetToString(state.pkt_rx.buf, pkt, num_bytes);
			state.pkt_rx.size = num_bytes;

			if( num_bytes >= 6 ) //min packet size
			{
				//parse packet data
				uint8_t  func     = pkt[1];
				uint16_t offset   = pkt[2] << 8 | pkt[3];

				//generate response in pkt
				if( func == 3 && num_bytes == 6 )
					num_bytes = handleModbusFunc3(pkt, offset);
				else if( func == 6 && num_bytes == 6 )
					num_bytes = handleModbusFunc6(pkt, offset);
				else
					num_bytes = handleModbusException(pkt, MODBUS_EXCEPTION_BAD_FUNC);

				if( num_bytes > 0 )
				{
					packetToString(state.pkt_tx.buf, pkt, num_bytes);
					state.pkt_tx.size = num_bytes;

					//update length field in MBAP header
					uint16_t len = num_bytes;
					buf[4] = highByte(len);
					buf[5] = lowByte(len);

					client.write(buf, num_bytes+6);
				}
			}
		}
	}
}

/*
 * Returns true if the Ethernet port is up and has an address
 */
bool isEthernetSetup()
{
	return state.network_configured;
}

/*
 * Setup the Ethernet port and request an address via DHCP
 */
void setupEthernet(uint8_t* mac)
{
	int status = Ethernet.begin(mac, 8000, 4000);

	if( status == 1 )
	{
		Serial.print("DHCP connected, IP: ");
		Serial.println( Ethernet.localIP() );

		server = EthernetServer(config.modbus_tcp_port);
		server.begin();
	}
	else
	{
		Serial.println("DHCP request failed");
	}

	state.network_configured = status;
}

/*
 * Handle Ethernet config and incoming modbus-tcp requests
 */
void manageEthernet()
{
	if( ! isEthernetSetup() )
	{
		int status = loadMAC(state.mac_addr, EEPROM_SIZE);

		if( status != 0 )
		{
			if( numRandomBytes() >= 5 )
			{
				//LAA MAC
				state.mac_addr[0] = 0x2;
				state.mac_addr[1] = getRandomByte();
				state.mac_addr[2] = getRandomByte();
				state.mac_addr[3] = getRandomByte();
				state.mac_addr[4] = getRandomByte();
				state.mac_addr[5] = getRandomByte();

				status = saveMAC(state.mac_addr, EEPROM_SIZE);
			}
		}
		else
		{
			static uint32_t tm = 999999;  //trigger the first timeout immediately

			uint32_t timeout = uint32_t(config.dhcp_frequency) * 1000;

			if( timeout != 0 )
			{
				if( millis() - tm > timeout )
				{
					tm = millis();
					setupEthernet(state.mac_addr);
				}
			}
		}
	}
	else
	{
		Ethernet.maintain();

		manageModbusTCP();
	}
}

/*
 * System setup
 */
void setup()
{
	setupInputs();
	setupOutputs();
	setupRelays();

	setupSerialUI();

	int status = loadConfig();
	if( status != 0 )
		Serial.println("failed to load config");
}

/*
 * System loop
 */
void loop()
{
	readAllInputs();

	//allow modifying input values via modbus
	manageEthernet();

	manageLightingSystem();

	writeRelayOutputs();

	writeDigitalOutputs();

	//this must be before handling serial data
	if( Serial.available() )
		gatherEntropy();

	manageSerialUI();

	delay(10);
}

