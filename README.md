This project implements a lighting controller which allows smarter control of your building lighting systems.

Hardware:
- The code is currently designed to run on a Controllino Mega.
- This is an Arduino like PLC with 21 inputs and 16 relays.
- It costs ~€350 incl. VAT & Shipping.

Wiring:
- Light switches are run on 12 volts and feed into a PLC digital input
- The live wire from the fuse board to the lighting circuit is connected through a relay controlled by the PLC
- The neutral wire goes directly from the lighting circuit back to the fuse board

Benefits:
- Remapping light switches to control other lights
- Integration with smoke alarms and security systems
- Timed or remote control of lights
